#!/usr/bin/bash

export PUBLIC_IP="x.x.x.x"
export SUBDOMAIN="xxxxx"
export DOMAIN="xxxx"
export USERNAME="xxxx"
export NC_DDNS_PASS="xxxxx"
export TROJAN_GO_PASS="xxxxx"
export SSH_KEY="xxxxx"

# Update Namecheap DNS
echo '--------------- Update DNS --------------'
curl -v "https://dynamicdns.park-your-domain.com/update?host=${SUBDOMAIN}&domain=${DOMAIN}&password=${NC_DDNS_PASS}&ip=${PUBLIC_IP}"

ansible-playbook -i $PUBLIC_IP,  --key-file "~/.ssh/$SSH_KEY" --extra-vars "username=$USERNAME domain_name=$DOMAIN subdomain_name=$SUBDOMAIN public_ip=$PUBLIC_IP namecheap_ddns_password=$NC_DDNS_PASS trojan_go_password=$TROJAN_GO_PASS" stargate_playbook.yml
