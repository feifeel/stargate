# stargate


## Install
You have 2 ways to install it:
1. You have an VM already setup in the cloud
2. You Deploy it via Terraform on AWS



### 1. Existing VM
In this scenario, you already have a Ubuntu VM ready running in any Cloud provider


#### Requirements
You need to install Ansible on your computer

##### Install Ansible with pipenv 
```bash
cd ansible
# It will install the required python version
pipenv shell

# It will install the required python packages
pipenv install
```

##### Install Ansible with python
```bash
cd ansible
python3 -m venv ~/.pyenv

source ~/.pyenv/bin/activate

pip3 install -U pip
pip3 install ansible
```

#### Deploy
```bash
# Edit run_playbook.sh with your variables
vim run_playbook.sh

# Run it
./run_playbook.sh
```


### 2. Terraform and Cloud Providers

#### Requirements
You need to install Terraform on your computer


#### Deploy 
```bash
# Depends on wich Compute service
# cd terraform/aws/ec2 
# cd terraform/aws/lightsail
# cd terraform/gcp/gce

terraform init

# Edit defaults.tfvars with your variables
tfvar_file=defaults.tfvars

# Apply
terraform plan -var-file=$tfvar_file
terraform apply -var-file=$tfvar_file

# Destroy
terraform destroy -var-file=$tfvar_file  -auto-approve
```