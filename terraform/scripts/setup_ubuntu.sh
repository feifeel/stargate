#!/usr/bin/env bash

PUBLIC_IP="${public_ip}"
SUBDOMAIN="${subdomain_name}"
DOMAIN="${domain_name}"
USERNAME="${username}"
NC_DDNS_PASS="${namecheap_ddns_password}"
TROJAN_GO_PASS="${trojan_go_password}"
PLAYBOOK_BRANCH="main"

# minimum setup
apt-get update  >> /opt/setup.log
apt-get install --no-install-recommends -y \
    ansible \
    curl \
    tree  >> /opt/setup.log

echo "PUBLIC_IP=$PUBLIC_IP" >> /opt/setup.log

# Update Namecheap DNS
echo '--------------- Update DNS --------------' >> /opt/setup.log
curl -v "https://dynamicdns.park-your-domain.com/update?host=$SUBDOMAIN&domain=$DOMAIN&password=$NC_DDNS_PASS&ip=$PUBLIC_IP"  >> /opt/setup.log

# Download Playbook
curl \
    -L https://gitlab.com/feifeel/stargate/-/raw/$PLAYBOOK_BRANCH/ansible/stargate_playbook.yml \
    -o /opt/stargate_playbook.yml  >> /opt/setup.log 


cat /opt/stargate_playbook.yml >> /opt/setup.log

ansible-playbook --connection=local --inventory '127.0.0.1,' --limit 127.0.0.1 --extra-vars "username=$USERNAME domain_name=$DOMAIN subdomain_name=$SUBDOMAIN public_ip=$PUBLIC_IP namecheap_ddns_password=$NC_DDNS_PASS trojan_go_password=$TROJAN_GO_PASS" /opt/stargate_playbook.yml  >> /opt/setup.log


echo "sleep 20" >> /opt/setup.log
sleep 20
docker ps >> /opt/setup.log



########## Create a new script for auto-restart

echo "Create stargate_reboot.sh" >> /opt/setup.log

START_SCRIPT=/opt/stargate_reboot.sh
echo '#!/bin/bash' > $START_SCRIPT

echo "export DOMAIN=$DOMAIN" >> $START_SCRIPT
echo "export SUBDOMAIN=$SUBDOMAIN" >> $START_SCRIPT
echo "export NC_DDNS_PASS=$NC_DDNS_PASS" >> $START_SCRIPT
echo "echo ''" >> $START_SCRIPT

echo 'echo "Get public IP"' >> $START_SCRIPT
echo 'PUB_IP=$(curl -s http://ipinfo.io/ip)' >> $START_SCRIPT
echo 'echo "PUB_IP=$PUB_IP"' >> $START_SCRIPT
echo "echo ''" >> $START_SCRIPT


echo 'echo "--------------- Update DNS --------------"' >> $START_SCRIPT
echo "export URL='https://dynamicdns.park-your-domain.com/update?host=$SUBDOMAIN&domain=$DOMAIN&password=$NC_DDNS_PASS&ip='" >> $START_SCRIPT
echo "curl "\$\{URL\}\$\{PUB_IP\}" " >> $START_SCRIPT
echo "echo ''" >> $START_SCRIPT
echo "echo ''" >> $START_SCRIPT

echo "echo Get DNS IP" >> $START_SCRIPT
echo 'DNS_IP=$(dig +short $SUBDOMAIN.$DOMAIN)' >> $START_SCRIPT
echo 'echo "DNS_IP=$DNS_IP"' >> $START_SCRIPT
echo "echo ''" >> $START_SCRIPT

echo 'echo "Compare IPs"' >> $START_SCRIPT
echo 'if [ "$PUB_IP" != "$DNS_IP" ]; then' >> $START_SCRIPT
echo '   for i in {1..60}' >> $START_SCRIPT
echo '    do' >> $START_SCRIPT
echo '        echo "PUB_IP $PUB_IP" != "DNS_IP $DNS_IP"' >> $START_SCRIPT
echo '        echo "Iteration $i"' >> $START_SCRIPT
echo '        sleep 10' >> $START_SCRIPT
echo '        DNS_IP=$(dig +short $SUBDOMAIN.$DOMAIN)' >> $START_SCRIPT
echo '        if [ "$PUB_IP" == "$DNS_IP" ]; then' >> $START_SCRIPT
echo '           echo "Bingo!"' >> $START_SCRIPT
echo '            exit 0' >> $START_SCRIPT
echo '        fi' >> $START_SCRIPT
echo '    done' >> $START_SCRIPT
echo '    echo "Public IP and DIG IP are still not equal after 12 attempts. Exiting."' >> $START_SCRIPT
echo '    exit 1' >> $START_SCRIPT
echo 'else' >> $START_SCRIPT
echo '    echo "Bingo!"' >> $START_SCRIPT
echo 'fi' >> $START_SCRIPT

echo "echo ''" >> $START_SCRIPT

echo 'echo "--------------- Restart Trojan Caddy ------------"
cd /opt/trojan-go-caddy
docker-compose -f docker-compose_trojan-go.yml down
sleep 5
docker-compose -f docker-compose_trojan-go.yml up -d
sleep 10
docker-compose -f docker-compose_trojan-go.yml ps' >> $START_SCRIPT
echo ""

echo "$START_SCRIPT content" >> /opt/setup.log
cat $START_SCRIPT >> /opt/setup.log
echo "" >> /opt/setup.log

chmod +x $START_SCRIPT



echo "Make stargate_reboot.sh executable" >> /opt/setup.log


# Create systemd service unit file for script2
cat <<'EOF' > /etc/systemd/system/stargate_reboot.service
[Unit]
Description=stargate_reboot

[Service]
ExecStart=/opt/stargate_reboot.sh
Type=oneshot

[Install]
WantedBy=multi-user.target
EOF

# Reload systemd daemon
systemctl daemon-reload

# Enable and start stargate_reboot.service
systemctl enable stargate_reboot.service
# systemctl start stargate_reboot.service

echo "systemctl status stargate_reboot.service" >> /opt/setup.log
systemctl status stargate_reboot.service >> /opt/setup.log
echo "It should not start yet" >> /opt/setup.log