# -------------------------------------------------------------------------------------------
# Variables
# -------------------------------------------------------------------------------------------

variable "aws_profile" {
    type = string
    default = "default"
}

variable "aws_conf_file_path" {
    type = string
}

variable "aws_cred_file_path" {
    type = string
}

variable "regions" {
    type = map
    default = {
        singapore = "ap-southeast-1"
        japan = "ap-northeast-1"
        korea = "ap-northeast-2"
        india = "ap-south-1"
    }
}

variable "selected_country" {
  type = string
}

variable "zones" {
    type = map
    default = {
        a = "a" # support all
        b = "b" # except japan
        c = "c" # support all
        d = "d" # support only korea, japan
    }
}

variable "selected_zone" {
    type = string
}

variable "instance_name_prefix" {
    default = "lightsail"
}

variable "instance_customizable_name" {
    type = string
}

variable "machine_config" {
    type = map
    default = {
        os = "ubuntu_22_04"
        nonroot_username = "ubuntu"
    }
}

variable "instance_type" {
   default = "micro_3_0"  # india uses nano_2_1 instead
}

variable "ssh_public_key_path" {
    type = string
    description = "your ssh public key for importing to lightsail"
}

variable "ssh_private_key_path" {
    type = string
    description = "your ssh private key for connecting to lightsail vm after deployment"
}

variable "domain_name" {
    type = string
}

variable "subdomain_name" {
    type = string
}

variable "namecheap_ddns_password" {
    type = string
    sensitive = true
}

variable "trojan_go_password" {
    type = string
    sensitive = true
}



# -------------------------------------------------------------------------------------------
# Provider
# -------------------------------------------------------------------------------------------
provider "aws" {
  region = var.regions[var.selected_country]
  profile = var.aws_profile
  shared_config_files = [var.aws_conf_file_path]
  shared_credentials_files = [var.aws_cred_file_path]
}


# -------------------------------------------------------------------------------------------
# Resources
# -------------------------------------------------------------------------------------------


locals {
  instance_name = "${var.instance_name_prefix}-${var.selected_country}-${var.zones[var.selected_zone]}-${var.instance_customizable_name}"
}

resource "aws_lightsail_static_ip_attachment" "lightsail_instance_ip_attachment" {
  static_ip_name = aws_lightsail_static_ip.instance_ip.id
  instance_name  =  aws_lightsail_instance.lightsail_instance.id
}

resource "aws_lightsail_static_ip" "instance_ip" {
  name = "${local.instance_name}-ip"
}

resource "aws_lightsail_key_pair" "ssh" {
  name       = "key-${local.instance_name}"
  public_key = file(var.ssh_public_key_path)
}

resource "aws_lightsail_instance" "lightsail_instance" {
  name              = local.instance_name
  availability_zone = "${var.regions[var.selected_country]}${var.zones[var.selected_zone]}"
  blueprint_id      = var.machine_config["os"]
  bundle_id         = var.instance_type
  key_pair_name     = aws_lightsail_key_pair.ssh.name
  user_data         = templatefile(
    "${path.root}/setup_ubuntu.sh.tftpl",
    {
      username = var.machine_config["nonroot_username"],
      domain_name = var.domain_name,
      subdomain_name = var.subdomain_name,
      public_ip = aws_lightsail_static_ip.instance_ip.ip_address,
      namecheap_ddns_password = var.namecheap_ddns_password,
      trojan_go_password = var.trojan_go_password
    }
  )
}

resource "aws_lightsail_instance_public_ports" "proxy" {
  instance_name = aws_lightsail_instance.lightsail_instance.name

  port_info {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
  }
  port_info {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
  }
  port_info {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
  }
  port_info {
    protocol    = "udp"
    from_port   = 60000
    to_port     = 60010
  }
  port_info {
    protocol    = "tcp"
    from_port   = 8990
    to_port     = 8990
  }
  port_info {
    protocol    = "udp"
    from_port   = 8990
    to_port     = 8990
  }
}



# -------------------------------------------------------------------------------------------
# Output
# -------------------------------------------------------------------------------------------


output "public_ip_address" {
    value = aws_lightsail_static_ip.instance_ip.ip_address
}

output "hostname" {
    value = "${var.subdomain_name}.${var.domain_name}"
}

output "username" {
    value = var.machine_config["nonroot_username"]
}

output "ssh_key_pair_name" {
    value = aws_lightsail_key_pair.ssh.name
}

output "ssh-connect" {
    value = "ssh -i ${trimsuffix(var.ssh_public_key_path, ".pub")} ${var.machine_config["nonroot_username"]}@${aws_lightsail_static_ip.instance_ip.ip_address}"
}

output "mosh-connect" {
   value = "mosh --ssh='ssh -i ${trimsuffix(var.ssh_public_key_path, ".pub")}' ${var.machine_config["nonroot_username"]}@${aws_lightsail_static_ip.instance_ip.ip_address}"
}
