# -------------------------------------------------------------------------------------------
# Variables
# -------------------------------------------------------------------------------------------
variable "aws_profile" {
    type = string
    default = "default"
}

variable "aws_conf_file_path" {
    type = string
}

variable "aws_cred_file_path" {
    type = string
}



variable "selected_country" {
  type = string
}

variable "regions" {
    type = map
    default = {
        singapore = "ap-southeast-1"
        japan = "ap-northeast-1"
        korea = "ap-northeast-2"
        india = "ap-south-1"
    }
}

variable "selected_zone" {
    type = string
}

variable "ssh_public_key_path" {
    type = string
    description = "your ssh public key for importing to stargate"
}

variable "ssh_private_key_path" {
    type = string
    description = "your ssh private key for connecting to stargate vm after deployment"
}

variable "domain_name" {
    type = string
}

variable "subdomain_name" {
    type = string
}

variable "machine_config" {
    type = map
    default = {
        os = "ubuntu_22_04"
        nonroot_username = "ubuntu"
        image = "ami-0be48b687295f8bd6" # Ubuntu 22.04 LTS (this may change, please verify from AWS console)
    }
}

variable "instance_type" {
   default = "t2.micro"
}

variable "namecheap_ddns_password" {
    type = string
    sensitive = true
}

variable "trojan_go_password" {
    type = string
    sensitive = true
}


# -------------------------------------------------------------------------------------------
# Provider
# -------------------------------------------------------------------------------------------
provider "aws" {
  region = var.regions[var.selected_country]
  profile = var.aws_profile
  shared_config_files = [var.aws_conf_file_path]
  shared_credentials_files = [var.aws_cred_file_path]
}


# -------------------------------------------------------------------------------------------
# Resources
# -------------------------------------------------------------------------------------------

resource "aws_key_pair" "my_key" {
  key_name   = "my_key"
  public_key = file("${var.ssh_public_key_path}")
}

resource "aws_security_group" "allow_ssh_http_https" {
  name        = "allow_ssh_http_https"
  description = "Allow SSH, HTTP and HTTPS inbound traffic"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_ssh_http_https"
  }
}


# Create an Elastic IP
resource "aws_eip" "stargate_eip" {}

resource "aws_instance" "stargate" {
  ami           = var.machine_config["image"]
  instance_type = var.instance_type
  availability_zone = "${var.regions[var.selected_country]}${var.selected_zone}"
  key_name = "my_key"

  vpc_security_group_ids = [aws_security_group.allow_ssh_http_https.id]

  tags = {
    Name = var.subdomain_name
  }

  # Install Ansible, download a playbook and run it
  user_data         = templatefile(
    "${path.root}/setup_ubuntu.sh.tftpl",
    {
      username = var.machine_config["nonroot_username"],
      domain_name = var.domain_name,
      subdomain_name = var.subdomain_name,
      public_ip = aws_eip.stargate_eip.public_ip,
      namecheap_ddns_password = var.namecheap_ddns_password,
      trojan_go_password = var.trojan_go_password
    }
  )

}


# Associate the Elastic IP with the EC2 instance
resource "aws_eip_association" "stargate_eip_association" {
  instance_id = aws_instance.stargate.id
  allocation_id = aws_eip.stargate_eip.id
}


# -------------------------------------------------------------------------------------------
# Output
# -------------------------------------------------------------------------------------------

output "public_ip" {
    value = aws_eip.stargate_eip.public_ip
}

output "connect" {
    value = "ssh -i ${trimsuffix(var.ssh_private_key_path, ".pub")} ${var.machine_config["nonroot_username"]}@${aws_eip.stargate_eip.public_ip}"
}

output "mosh-connect" {
   value = "mosh --ssh='ssh -i ${trimsuffix(var.ssh_private_key_path, ".pub")}' ${var.machine_config["nonroot_username"]}@${aws_eip.stargate_eip.public_ip}"
}

# resource "aws_network_interface_sg_attachment" "stargate" {
#   security_group_id    = aws_security_group.allow_ssh_http_https.id
#   network_interface_id = aws_instance.stargate.primary_network_interface_id
# }


