# -------------------------------------------------------------------------------------------
# Variables
# -------------------------------------------------------------------------------------------
variable "gcp_project" {
    type = string
    default = "default"
}

variable "gcp_cred_file_path" {
    type = string
}

variable "selected_country" {
  type = string
}

variable "regions" {
    type = map
    default = {
        singapore = "asia-southeast1"
        taiwan = "asia-east1"
        tokyo = "asia-northeast1"
        osaka = "asia-northeast2"
    }
}

variable "selected_zone" {
    type = string
}

variable "ssh_public_key_path" {
    type = string
    description = "your ssh public key for importing to lightsail"
}

variable "ssh_private_key_path" {
    type = string
    description = "your ssh private key for connecting to lightsail vm after deployment"
}

variable "domain_name" {
    type = string
}

variable "subdomain_name" {
    type = string
}

variable "machine_config" {
    type = map
    default = {
        os = "ubuntu_22_04"
        nonroot_username = "ubuntu"
        image = "ubuntu-2204-jammy-v20240904"
    }
}

variable "instance_type" {
   default = "g1-small"
}

variable "namecheap_ddns_password" {
    type = string
    sensitive = true
}

variable "trojan_go_password" {
    type = string
    sensitive = true
}


# -------------------------------------------------------------------------------------------
# Provider
# -------------------------------------------------------------------------------------------
provider "google" {
  credentials = file("${var.gcp_cred_file_path}")
  project     = var.gcp_project
  region      = var.regions[var.selected_country]
}

# -------------------------------------------------------------------------------------------
# Resources
# -------------------------------------------------------------------------------------------

# Create a static public IP
resource "google_compute_address" "static_ip" {
  name   = "${var.subdomain_name}-ip"
  region = var.regions[var.selected_country]
}

# Create a compute instance and attach the static IP
resource "google_compute_instance" "stargate" {
  name         = var.subdomain_name
  machine_type = var.instance_type
  zone         = "${var.regions[var.selected_country]}-${var.selected_zone}"

  boot_disk {
    initialize_params {
      image = var.machine_config["image"]
    }
  }

  network_interface {
    network = "default"
    access_config {
      nat_ip = google_compute_address.static_ip.address  # Attach the static IP
    }
  }

  metadata = {
    ssh-keys = "${var.machine_config["nonroot_username"]}:${file("${var.ssh_public_key_path}")}"
  }

  tags = ["http-server", "https-server"]

  provisioner "local-exec" {
    command = "echo ${google_compute_instance.stargate.network_interface.0.access_config.0.nat_ip}"
  }

  metadata_startup_script = templatefile("${path.root}/setup_ubuntu.sh.tftpl", 
    {
      username = var.machine_config["nonroot_username"],
      domain_name = var.domain_name,
      subdomain_name = var.subdomain_name,
      public_ip = google_compute_address.static_ip.address,
      namecheap_ddns_password = var.namecheap_ddns_password,
      trojan_go_password = var.trojan_go_password
    }
  )
}

# -------------------------------------------------------------------------------------------
# Output
# -------------------------------------------------------------------------------------------

output "public_ip" {
  value = google_compute_address.static_ip.address
}

output "connect" {
    value = "ssh -i ${trimsuffix(var.ssh_public_key_path, ".pub")} ${var.machine_config["nonroot_username"]}@${google_compute_address.static_ip.address}"
}